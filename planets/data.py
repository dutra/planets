# J2000
# https://nssdc.gsfc.nasa.gov/planetary/planetfact.html

sun = {
    'color': '#f1c40f'
}

planets = {
    # https://nssdc.gsfc.nasa.gov/planetary/factsheet/mercuryfact.html
    'mercury': {
        'orbital_period': 0.241,
        'orbital_eccentricity': 0.2056,
        'length_semi_major_axis': 0.38709893,
        'orbital_inclination': 7.00487,
        'ecliptic_longitude_epoch': 252.25084,
        'ecliptic_longitude_perihelion': 77.45645,
        'ecliptic_longitude_ascending_node': 48.33167,
        'color': '#d35400'
    },
    # https://nssdc.gsfc.nasa.gov/planetary/factsheet/venusfact.html
    'venus': {
        'orbital_period': 0.615197,
        'orbital_eccentricity': 0.0067767,
        'length_semi_major_axis': 0.723336,
        'orbital_inclination': 3.394676,
        'ecliptic_longitude_epoch': 181.979100,
        'ecliptic_longitude_perihelion': 131.602467,
        'ecliptic_longitude_ascending_node': 76.679843,
        'color': '#f39c12'
    },
    'earth': {
        'orbital_period': 1.000017,
        'orbital_eccentricity': 0.0167112,
        'length_semi_major_axis': 1.000003,
        'orbital_inclination': -0.000015,
        'ecliptic_longitude_epoch': 100.464572,
        'ecliptic_longitude_perihelion': 102.937682,
        'ecliptic_longitude_ascending_node': 0.0,
        'color': '#2980b9'
        },
    'mars': {
        'orbital_period': 1.880848,
        'orbital_eccentricity': 0.093394,
        'length_semi_major_axis': 1.523710,
        'orbital_inclination': 1.849691,
        'ecliptic_longitude_epoch': -4.553432,
        'ecliptic_longitude_perihelion': -23.943630,
        'ecliptic_longitude_ascending_node': 49.559539,
        'color': '#e74c3c'
    },
    # https://nssdc.gsfc.nasa.gov/planetary/factsheet/jupiterfact.html
    'jupiter': {
        'orbital_period': 11.862,
        'orbital_eccentricity': 0.0489,
        'length_semi_major_axis': 5.2044,
        'orbital_inclination': 1.30530,
        'ecliptic_longitude_epoch': 34.40438,
        'ecliptic_longitude_perihelion': 14.75385,
        'ecliptic_longitude_ascending_node': 100.55615,
        'color': '#fff176'
    },
    # https://nssdc.gsfc.nasa.gov/planetary/factsheet/saturnfact.html
    'saturn': {
        'orbital_period': 29.457,
        'orbital_eccentricity': 0.0565,
        'length_semi_major_axis': 9.53707032,
        'orbital_inclination': 2.48446,
        'ecliptic_longitude_epoch': 49.94432,
        'ecliptic_longitude_perihelion': 92.43194,
        'ecliptic_longitude_ascending_node': 113.71504,
        'color': '#f39c12'
    }
}
