import sys
from datetime import datetime, timedelta, timezone
import random

# import matplotlib.pyplot as plt
from math import radians, pi, cos, sin

from PyQt5.QtWidgets import qApp, QApplication, QLabel, QMainWindow, QWidget, QHBoxLayout, QVBoxLayout
from PyQt5.QtGui import QPainter, QPen, QBrush, QColor, QPainterPath
from PyQt5.QtCore import Qt, QPointF, QTimer

from planets.kepler import get_planet_path, get_planet_position, get_planet_semi_minor_axis, info
from planets import data

MAX_R = 12.0

class MainWidget(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):

        self.dt = datetime.now(timezone.utc)


        self.label = QLabel('Current Time')
        self.label.setStyleSheet("QLabel { background-color: white; color: black; }");
        self.label.adjustSize()
        self.pp = PlanetPainter()

        vbox = QVBoxLayout()
        self.setLayout(vbox)
        # hbox = QHBoxLayout()
        # vbox.addLayout(hbox)

        # hbox.addStretch(1)

        vbox.addWidget(self.pp, 9)
        vbox.addWidget(self.label, 1, Qt.AlignBottom)
        # vbox.addStretch(1)


        self.timer = QTimer()

        self.step = 100
        self.timer.timeout.connect(self.timeStep)
        self.timer.start(self.step)

        self.setGeometry(300, 300, 300, 190)
        self.setWindowTitle('Points')
        self.show()

    def timeStep(self):
        self.dt = self.dt + timedelta(days=7*self.step/1000)

        self.pp.setDateTime(self.dt)
        self.label.setText(self.dt.isoformat())

        self.update()


class PlanetPainter(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        self.dt = datetime.now(timezone.utc)

        self.setGeometry(300, 300, 300, 190)
        self.setWindowTitle('Points')

        self.show()

    def setDateTime(self, dt):
        self.dt = dt

    def paintEvent(self, e):
        qp = QPainter()

        qp.begin(self)

        qp.setRenderHint(QPainter.Antialiasing)

        self.drawPlanets(qp, self.dt)

        qp.end()

    def drawPlanets(self, qp, dt):
        qp.setPen(QColor("#00000000"))
        size = self.size()

        if size.height() <= 1 or size.height() <= 1:
            return

        # qp.setPen(QPen(Qt.green,  8, Qt.SolidLine))

        # sun
        self.drawPlanet(qp, 0, 0, QColor(data.sun['color']))

        for name, planet in data.planets.items():
            coords = get_planet_position(dt, planet)
            r = coords[0]
            longitude = coords[2]

            positions = get_planet_path(planet, 120)

            self.drawPlanet(qp, r, longitude, QColor(planet['color']))

            self.drawPlanetPath(qp, positions, QColor(planet['color']))




    def scaleXY(self, x, y):
        size = self.size()
        hw = size.width()/2.0
        hh = size.height()/2.0

        x = x * hw
        y = y * hh

        return (x, y)


    def convertXY(self, x, y):
        size = self.size()
        hw = size.width()/2.0
        hh = size.height()/2.0

        nx = hw + x*hw
        ny = hh + y*hh

        return QPointF(nx, ny)

    def drawPlanet(self, qp, r, longitude, color):

        x = r/MAX_R * cos(radians(longitude))
        y = r/MAX_R * sin(radians(longitude))

        qp.setPen(QPen(QColor("#00000000"), 8, Qt.SolidLine))
        qp.setBrush(QBrush(color, Qt.SolidPattern))
        p = self.convertXY(x, y)
        qp.drawEllipse(p, 10, 10)


    def drawPlanetPath(self, qp, positions, color):
        path = QPainterPath()
        p = positions[0]
        r = p[0]
        l = p[2]
        x = r/MAX_R * cos(radians(l))
        y = r/MAX_R * sin(radians(l))

        p = self.convertXY(x, y)
        path.moveTo(p)

        for p in positions[1:]:
            r = p[0]
            l = p[2]
            x = r/MAX_R * cos(radians(l))
            y = r/MAX_R * sin(radians(l))

            p = self.convertXY(x, y)

            path.lineTo(p)
            # qp.drawEllipse(p, 2, 2)

        qp.setPen(QPen(color, 2, Qt.SolidLine))
        qp.setBrush(QBrush(QColor("#00000000"), Qt.SolidPattern))
        # qp.setBrush(QBrush(color, Qt.SolidPattern))
        qp.drawPath(path)

# Subclass QMainWindow to customise your application's main window
class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.setStyleSheet("background-color: #2c3e50;")
        self.setWindowTitle("My Awesome App")

        mw = MainWidget()

        self.setCentralWidget(mw)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Q:
            qApp.quit()


def plot_degrees(ax, r, theta, *args, **kwargs):
    ax.plot(radians(theta), r, *args, **kwargs)

def plot_planets_matplotlib(dt):
    venus_coords = get_planet_position(dt, data['venus'])
    print(f"Venus: {str(venus_coords)}")

    earth_coords = get_planet_position(dt, data['earth'])
    print(f"Earth: {str(earth_coords)}")

    mars_coords = get_planet_position(dt, data['mars'])
    print(f"Mars: {str(mars_coords)}")

    ax = plt.subplot(111, projection='polar')

    plot_degrees(ax, 0, 0, 'yo')

    plot_degrees(ax, venus_coords[0], venus_coords[2], 'yo')
    plot_degrees(ax, earth_coords[0], earth_coords[2], 'bo')
    plot_degrees(ax, mars_coords[0], mars_coords[2], 'ro')

    plot_degrees(ax, 1.8, 0, 'gv')
    plot_degrees(ax, 1.8, 180, 'r2')

    ax.set_rmax(2)
    ax.set_rticks([0.5, 1, 1.5, 2])  # less radial ticks
    ax.set_rlabel_position(-22.5)  # get radial labels away from plotted line
    # ax.set_thetalim(2*pi, 0)
    ax.grid(True)
    plt.show()


def init():
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    app.exec_()
