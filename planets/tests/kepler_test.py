from datetime import datetime, timezone
import pytest
from planets import kepler
from planets.data import data

def test_get_julian():
    dt = datetime(2016, 1, 4, 3, 0)
    assert kepler.get_julian(dt) == 2457391.625

def test_get_days_since_epoch():
    dt = datetime(2016, 1, 4, 3, 0)
    assert kepler.get_days_since_epoch(dt) == 5846.625

def test_get_planet_position():
    dt = datetime(2016, 1, 4, 3, 0)
    venus_coords = kepler.get_planet_position(dt, data['venus'])

    assert venus_coords == pytest.approx((0.720735, 3.119613, 189.891413), abs=0.1)
