import math
from math import radians, degrees, pi, sqrt
from datetime import datetime
from planets import data

def wrap_angle(angle):
    angle -= (360.0 * math.floor(angle/360.0))
    if angle < 0:
        angle *= -1

    return angle
    # return angle % 360.0 - (360.0 - angle)


def sin(angle):
    return math.sin(radians(angle))


def asin(value):
    return wrap_angle(degrees(math.asin(value)))


def cos(angle):
    return math.cos(radians(angle))


def get_julian(dt):
    day = dt.toordinal() + 1721424.5
    t = dt.time()
    time = t.hour/24.0 + t.minute/(24.0*60.0) + t.second/(24.0*60.0*60.0)
    return day + time


def get_days_since_epoch(dt):
    return get_julian(dt) - 2451545.0


def get_sun(ts):
    return (0, 0)

def get_planet_path(planet_data, steps):
    orbital_period = planet_data['orbital_period']
    orbital_eccentricity = planet_data['orbital_eccentricity']
    length_semi_major_axis = planet_data['length_semi_major_axis']
    orbital_inclination = planet_data['orbital_inclination']
    ecliptic_longitude_epoch = planet_data['ecliptic_longitude_epoch']
    ecliptic_longitude_perihelion = planet_data['ecliptic_longitude_perihelion']
    ecliptic_longitude_ascending_node = planet_data['ecliptic_longitude_ascending_node']

    positions = []

    for i in range(steps+1):

        days = i/steps

        mean_anomaly =  360.0*days + ecliptic_longitude_epoch - ecliptic_longitude_perihelion

        true_anomaly = 360.0/pi * orbital_eccentricity * sin(mean_anomaly) + mean_anomaly

        ecliptic_longitude = wrap_angle(true_anomaly + ecliptic_longitude_perihelion)
        ecliptic_latitude = wrap_angle(asin(sin(ecliptic_longitude - ecliptic_longitude_ascending_node) * sin(orbital_inclination)))

        radius_vector = length_semi_major_axis*(1-orbital_eccentricity*orbital_eccentricity)/(1+orbital_eccentricity*cos(true_anomaly))

        positions.append((radius_vector, ecliptic_latitude, ecliptic_longitude))

    return positions


def get_planet_semi_minor_axis(planet_data):
    orbital_eccentricity = planet_data['orbital_eccentricity']
    length_semi_major_axis = planet_data['length_semi_major_axis']

    return length_semi_major_axis * sqrt(1 - orbital_eccentricity*orbital_eccentricity)


def get_planet_position(dt, planet_data):
    orbital_period = planet_data['orbital_period']
    orbital_eccentricity = planet_data['orbital_eccentricity']
    length_semi_major_axis = planet_data['length_semi_major_axis']
    orbital_inclination = planet_data['orbital_inclination']
    ecliptic_longitude_epoch = planet_data['ecliptic_longitude_epoch']
    ecliptic_longitude_perihelion = planet_data['ecliptic_longitude_perihelion']
    ecliptic_longitude_ascending_node = planet_data['ecliptic_longitude_ascending_node']

    days = get_days_since_epoch(dt)

    mean_anomaly = days * 360.0 / (365.242191 * orbital_period) + ecliptic_longitude_epoch - ecliptic_longitude_perihelion

    true_anomaly = 360.0/pi * orbital_eccentricity * sin(mean_anomaly) + mean_anomaly

    ecliptic_longitude = wrap_angle(true_anomaly + ecliptic_longitude_perihelion)
    ecliptic_latitude = wrap_angle(asin(sin(ecliptic_longitude - ecliptic_longitude_ascending_node) * sin(orbital_inclination)))

    radius_vector = length_semi_major_axis*(1-orbital_eccentricity*orbital_eccentricity)/(1+orbital_eccentricity*cos(true_anomaly))

    return (radius_vector, ecliptic_latitude, ecliptic_longitude)

def get_planet_semi_minor_axis(planet_data):
    orbital_eccentricity = planet_data['orbital_eccentricity']
    length_semi_major_axis = planet_data['length_semi_major_axis']

    return length_semi_major_axis * sqrt(1 - orbital_eccentricity*orbital_eccentricity)


def info(dt):
    print("--------- INFO --------")
    jd = get_julian(dt)

    print(f"Julian date: {jd}")

    print("** Planets **")

    for name, planet in data.planets.items():
        coords = get_planet_position(dt, planet)
        print(f"* {name}")
        print(f"  Radius: {coords[0]}")
        print(f"  Ecliptic Latitude: {coords[1]}")
        print(f"  Ecliptic Longitude: {coords[2]}")
