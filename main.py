from datetime import datetime, timezone
from planets import gui

from planets.kepler import get_planet_position, info


def main():
    dt = datetime.now(timezone.utc)
    info(dt)

    gui.init()


if __name__ == '__main__':
    main()
